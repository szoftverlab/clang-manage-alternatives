# Usage

This shell script helps to register already installed clang components to the
["alternatives system" of debian](https://wiki.debian.org/DebianAlternatives) and derived linuxes.
If you have installed clang/llvm components then run *uagen.sh*. It creates an
other shell script (named *reg-clang.sh*) that can be used to register just
already found clang/llvm programs into the "alternatives system".
*reg-clang.sh* will be created in the same directory where *uagen.sh* resides.

*uagen.sh* does not require super user right, but *reg-clang.sh* does. Generate
*reg-clang.sh*, review its content and register clang components by calling it.

For example if you want to register clang 12 at level 40 priority:

```sh
cd <somewhere>
./uagen.sh -v 12 -p 40
sudo ./reg-clang.sh
```